var $ = require('jquery');
var owlCarousel = require('owl.carousel');

module.exports = function () {

    var $steps = $('.steps-carousel');

    $steps.owlCarousel({
        margin: 0,
        items: 1,
        dots: false,
        nav: false,
        URLhashListener: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut'
    });
};
