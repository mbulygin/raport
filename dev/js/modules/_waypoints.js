var $ = require('jquery');
global.waypoint = require('waypoints');

module.exports = function () {

    var $summary = $('.js-summary'),
        $header = $('.site-header'),
        $hideble = $('.js-hideOnScroll'),
        $feedback = $('.feedback'),
        $sumOverall = $('.summary__overall'),
        $sumDetails = $('.summary__details');

    var hide = function () {
        $hideble.css('display', 'none');
    };

    var show = function () {
        $hideble.css('display', 'block');
    };

    $summary.waypoint(function (direction) {
        if (direction === 'down') {

            $sumDetails.animateCss('fadeOutLeft', hide);
            $sumOverall.animateCss('fadeOutRight', function () {
                $sumOverall.addClass('absolute').animateCss('fadeInUp');
            });
            $header.addClass('site-header--is-sticky');
            $summary.addClass('summary--is-scrolled');
            $feedback.css({
                paddingTop: '400px'
            });
        } else {
            $header.removeClass('site-header--is-sticky');
            $summary.removeClass('summary--is-scrolled');
            $sumOverall.animateCss('fadeOutDown', function () {
                show();
                $sumOverall.removeClass('absolute').animateCss('fadeIn');
            });

        }
    }, {
        offset: "-45px"
    });


    // $('.feedback-table').waypoint(function (direction) {
    //     if (direction === 'down') {
    //         $(this.element).animateCss('fadeInUp');
    //     }
    // }, {
    //     offset: '100%'
    // })
};
