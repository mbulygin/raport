global.jQuery = global.$ = require('jquery');
require('./modules/_animate-css');
require('../../node_modules/svg.js/dist/svg.min');
var svg4everybody = require('svg4everybody');
var waypoints = require('./modules/_waypoints');
var smoothScroll = require('./modules/_smooth-scroll');
var stepsOwl = require('./modules/_steps-carousel');

$(document).ready(function () {
    svg4everybody();
    waypoints();
    smoothScroll();
    stepsOwl();

    $('.js-hash-card__link').click(function () {
        $('.js-steps__navigation').find('.hash-card--is-active').removeClass('hash-card--is-active');
        $(this).parent().addClass('hash-card--is-active')
    });

    $('#showfeedback').click(function () {
        $(document).scrollTop(140);
    });

    $(window).scroll(function () {
        var s = $(window).scrollTop(),
            d = $(document).height(),
            c = $(window).height();
        var position = (s / (d - c)) * 100;

        $("#scrollFiller").css('width', position + '%');

    });

    var $summaryScroll = $('.summary__scroll');

    if ($summaryScroll) {
        $summaryScroll.animateCss('fadeInDown');
    }

    // $('.summary__overall, .summary__details').animateCss('fadeIn');

    var canvas = SVG('graph').size('100%', '75').viewbox(0, 0, 241.5, 62.7),
        path = canvas.path("M0,60.7C68.12,60.7,86.69,2,120.75,2c31,0,49.54,58.7,120.75,58.7").id('graphPath'),
        length = path.length(),
        result = canvas.path("M0,60.7C68.12,60.7,86.69,2,120.75,2c31,0,49.54,58.7,120.75,58.7").id('graphResult').stroke({
            dasharray: length,
            dashoffset: length
        });

    function getResultPathLength(perc) {
        return length - (perc * length / 100);
    }

    function animateNumber(elem, to, duration) {
        elem.prop('counter', 0).animate({
            counter: to
        }, {
            duration: duration,
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    }

    function animateResultUntil(scoreTotal, percent) {
        var VALUES = {
            duration: 1000
        };

        var pl = getResultPathLength(percent),
            dot = canvas.circle(8).id('graphDot'),
            $legend = $('#graphLegendVal'),
            $score = $('#scoreVal'),
            $meter = $('#resultsMeter'),
            compareScore = percent >100 ? 100 : percent;

        dot.animate(VALUES.duration, '<>').during(function (pos, morph, eased) {
            var p = path.pointAt(eased * (compareScore * length / 100));
            dot.center(p.x, p.y)
        });

        result.animate(VALUES.duration, '<>').attr('stroke-dashoffset', pl);

        animateNumber($legend, compareScore, VALUES.duration);
        animateNumber($score, scoreTotal, VALUES.duration);

        $meter.animate({
            height: compareScore + '%'
        }, {
            duration: VALUES.duration
        })
    }

    animateResultUntil(61, 83);

});